<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Multi profile field.
 *
 * @package    profilefield_multi
 * @copyright  2007 onwards Shane Elliot {@link http://pukunui.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace metasharedfieldtype_multi;

defined('MOODLE_INTERNAL') || die;

/**
 * Class local_metashared_field_multi
 *
 * @copyright  2007 onwards Shane Elliot {@link http://pukunui.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class metadata extends \local_metashared\fieldtype\metadata {

    /** @var array $options */
    public $options;

    /** @var int $datakey */
    public $datakey;
    
    /** @var int $fieldid */
    public $fieldid;

    /**
     * Constructor method.
     *
     * Pulls out the options for the multi from the database and sets the the corresponding key for the data if it exists.
     *
     * @param int $fieldid
     * @param int $instanceid
     * @param object $fielddata optional data for the field object.
     */
    public function __construct($fieldid = 0, $instanceid = 0, $fielddata = null) {
        // First call parent constructor.
        parent::__construct($fieldid, $instanceid, $fielddata);
        $this->fieldid = $fieldid;
        // Param 1 for multi type is the options.
        if (isset($this->field->param1)) {
            $options = explode("\n", $this->field->param1);
        } else {
            $options = [];
        }
        $this->options = [];
        if (!empty($this->field->required)) {
            $this->options[''] = get_string('choose').'...';
        }
        foreach ($options as $key => $option) {
            $this->options[$option] = format_string($option); // Multilang formatting with filters.
        }

        // Set the data key.
        if ($this->data !== null) {
            $key = $this->data;
            if (isset($this->options[$key]) || ($key = array_search($key, $this->options)) !== false) {
                $this->data = $key;
                $this->datakey = $key;
            }
        }
    }

    /**
     * Create the code snippet for this field instance
     * Overwrites the base class method
     * @param moodleform $mform Moodle form instance
     */
    public function edit_field_add($mform) {
        global $PAGE, $DB;
        
        /* //That code allows to to guess the box checked with just the answer, that's not a good solution when you several times the same anwser for different equation
        $metashared_field = $DB->get_record('local_metashared_field', array('id'=>$this->fieldid));
        $formules_tab = explode("\n", $metashared_field->param2);
        $preset_values = array();
       
        foreach($formules_tab as $mf) {
            
            if(strpos($this->data, "#>") !== false){
               $data = substr($this->data, 0, strpos($this->data, "#>"));
            } else {
                $data = $this->data;
            }
            
            if(strpos(explode("=", $mf)[1], "#>") !== false){
                $rr = substr(explode("=", $mf)[1], 0, strpos(explode("=", $mf)[1], "#>"));
            } else {
                $rr = explode("=", $mf)[1];
            }
            
            if ($rr == $data) {
                $formule= explode("=", $mf)[0];
                $preset_values= explode("+",$formule); 
            }
        }
        */
        $i = 0;
        foreach (explode("\n", $this->field->param1) as $op) {
            if ($i === 0) {
                $name = $this->field->name;
            } else {
                 $name = "";
            }

            $mform->addElement('advcheckbox', "mutli_".$this->inputname, $name, $op, 
                    array('level'=> $i,'class' => $this->inputname));
            
            $i++;
        }
        
        $checked="";
        $all_check = "";
        $preset_values = str_split(explode("=", $this->data)[0]);
        foreach ($preset_values as $pv) {
           $checked .=  "$( \"input[level*='".$pv."']\" ).filter('.".$this->inputname."').prop('checked', true);";
           $all_check .= $pv;
        }
        
        $mform->addElement('hidden',$this->inputname,$all_check); 
        
        if ($checked != "") {
            $ready_checked = "$( document ).ready(function() {".$checked."});";
        }
        
        $PAGE->requires->js_amd_inline("
        require(['jquery'], function($) {
        ".$ready_checked."
                    $( '.".$this->inputname."' ).click(function() {
                "."var list= document.getElementsByClassName(\"".$this->inputname."\");
                        var string_result = '';
                        for (var i = 0; i < list.length; i++) {
                            if (list[i].id === \"id_mutli_".$this->inputname."\") {
                                        if (list[i].checked == true) {
                                                string_result = string_result + list[i].getAttribute('level');
                                        }
                            }
                        }
                    document.getElementsByName(\"".$this->inputname."\")[0].value = string_result;    
                    "."
              });});
         ");
              
    }
        


    /**
     * Set the default value for this field instance
     * Overwrites the base class method.
     * @param moodleform $mform Moodle form instance
     */
    public function edit_field_set_default($mform) {
        $key = $this->field->defaultdata;
        if (isset($this->options[$key]) || ($key = array_search($key, $this->options)) !== false) {
            $defaultkey = $key;
        } else {
            $defaultkey = '';
        }
        $mform->setDefault($this->inputname, $defaultkey);
    }

    /**
     * The data from the form returns the key.
     *
     * This should be converted to the respective option string to be saved in database
     * Overwrites base class accessor method.
     *
     * @param mixed $data The key returned from the select input in the form
     * @param stdClass $datarecord The object that will be used to save the record
     * @return mixed Data or null
     */
    public function edit_save_data_preprocess($data, $datarecord) {
        return $data;
    }

    /**
     * When passing the instance object to the form class for the edit page
     * we should load the key for the saved data
     *
     * Overwrites the base class method.
     *
     * @param stdClass $instance Instance object.
     */
    public function edit_load_instance_data($instance) {
        $instance->{$this->inputname} = $this->datakey;
    }

    /**
     * HardFreeze the field if locked.
     * @param moodleform $mform instance of the moodleform class
     */
    public function edit_field_set_locked($mform) {
        if (!$mform->elementExists($this->inputname)) {
            return;
        }
        if ($this->is_locked() && !has_capability('moodle/user:update', context_system::instance())) {
            $mform->hardFreeze($this->inputname);
            $mform->setConstant($this->inputname, format_string($this->datakey));
        }
    }
    /**
     * Convert external data (csv file) from value to key for processing later by edit_save_data_preprocess
     *
     * @param string $value one of the values in multi options.
     * @return int options key for the multi
     */
    public function convert_external_data($value) {
        if (isset($this->options[$value])) {
            $retval = $value;
        } else {
            $retval = array_search($value, $this->options);
        }

        // If value is not found in options then return null, so that it can be handled
        // later by edit_save_data_preprocess.
        if ($retval === false) {
            $retval = null;
        }
        return $retval;
    }

    /**
     * Return the field type and null properties.
     * This will be used for validating the data submitted by a user.
     *
     * @return array the param type and null property
     * @since Moodle 3.2
     */
    public function get_field_properties() {
        return [PARAM_TEXT, NULL_NOT_ALLOWED];
    }
}


