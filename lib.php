<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die;
require_once($CFG->dirroot.'/local/metashared/metashared_form_copy_paste_entrepot.php');
/**
 * @package local_metashared
 * @author Mike Churchward <mike.churchward@poetopensource.org>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @copyright 2017, onwards Poet
 */

function local_metashared_supports($feature) {
    switch($feature) {
        case FEATURE_BACKUP_MOODLE2:
            return true;

        default:
            return null;
    }
}

/**
 * Loads user profile field data into the context object.
 * @param stdClass $user
 */
function local_metashared_load_data($instance, $contextlevel) {
   global $DB;

    if (is_object($instance) && isset($instance->id)) {
        $instanceid = $instance->id;
    } else {
        $instanceid = 0;
    }

    $sql = 'SELECT lmf.*, lm.instanceid, lm.fieldid, lm.data, lm.dataformat ';
    $sql .= 'FROM {local_metashared_field} lmf ';
    $sql .= 'LEFT JOIN {local_metashared} lm ON lmf.id = lm.fieldid AND lm.instanceid = :instanceid ';
    $sql .= 'WHERE lmf.contextlevel = :contextlevel ';

    $fields = $DB->get_records_sql($sql, ['instanceid' => $instanceid, 'contextlevel' => $contextlevel]);
    foreach ($fields as $field) {
        $newfield = "\\metasharedfieldtype_{$field->datatype}\\metadata";
        $formfield = new $newfield($field->id, $instanceid, $field);
        $formfield->edit_load_instance_data($instance);
    }
}

/**
 * Print out the customisable categories and fields for a users profile
 *
 * @param moodleform $mform instance of the moodleform class
 * @param int $instanceid id of user whose profile is being edited.
 */
function local_metashared_definition($mform, $instanceid = 0, $contextlevel) {
    global $DB;

    // If user is "admin" fields are displayed regardless.
    $update = has_capability('moodle/user:update', context_system::instance());

    if ($categories = $DB->get_records('local_metashared_category', ['contextlevel' => $contextlevel], 'sortorder ASC')) {
        foreach ($categories as $category) {
            if ($fields = $DB->get_records('local_metashared_field', ['categoryid' => $category->id], 'sortorder ASC')) {

                // Check first if *any* fields will be displayed.
                $display = false;
                foreach ($fields as $field) {
                    if ($field->visible != PROFILE_VISIBLE_NONE) {
                        $display = true;
                    }
                }

                // Display the header and the fields.
                if ($display || $update) {
                    $mform->addElement('header', 'category_'.$category->id, format_string($category->name));
                    foreach ($fields as $field) {
                        $newfield = "\\metasharedfieldtype_{$field->datatype}\\metadata";
                        $formfield = new $newfield($field->id, $instanceid);
                        $formfield->edit_field($mform);
                    }
                }
            }
        }
    }
}

/**
 * Adds profile fields to instance edit forms.
 * @param moodleform $mform
 * @param int $instanceid
 */
function local_metashared_definition_after_data($mform, $instanceid, $contextlevel) {
    global $DB;

    $instanceid = ($instanceid < 0) ? 0 : (int)$instanceid;

    if ($fields = $DB->get_records('local_metashared_field', ['contextlevel' => $contextlevel])) {
        foreach ($fields as $field) {
            $newfield = "\\metasharedfieldtype_{$field->datatype}\\metadata";
            $formfield = new $newfield($field->id, $instanceid);
            $formfield->edit_after_data($mform);
        }
    }
}

/**
 * Validates profile data.
 * @param stdClass $new
 * @param array $files
 * @return array
 */
function local_metashared_validation($new, $files, $contextlevel) {
    global $DB;

    if (is_array($new)) {
        $new = (object)$new;
    }

    $err = [];
    if ($fields = $DB->get_records('local_metashared_field', ['contextlevel' => $contextlevel])) {
        foreach ($fields as $field) {
            $newfield = "\\metasharedfieldtype_{$field->datatype}\\metadata";
            $formfield = new $newfield($field->id, $new->id);
            $err += $formfield->edit_validate_field($new, $files);
        }
    }
    return $err;
}

/**
 * Saves profile data for a instance.
 * @param stdClass $new
 */
function local_metashared_save_data($new, $contextlevel) {
    global $DB;

    if ($fields = $DB->get_records('local_metashared_field', ['contextlevel' => $contextlevel])) {
        foreach ($fields as $field) {
            $newfield = "\\metasharedfieldtype_{$field->datatype}\\metadata";
            $formfield = new $newfield($field->id, $new->id);
            $formfield->edit_save_data($new);
        }
    }
}

/**
 * Display profile fields.
 * @param int $instanceid
 */
function local_metashared_display_fields($instanceid, $contextlevel, $returnonly=false) {
    global $DB;

    $output = '';
    if ($categories = $DB->get_records('local_metashared_category', ['contextlevel' => $contextlevel], 'sortorder ASC')) {
        foreach ($categories as $category) {
            if ($fields = $DB->get_records('local_metashared_field', ['categoryid' => $category->id], 'sortorder ASC')) {
                foreach ($fields as $field) {
                    $newfield = "\\metasharedfieldtype_{$field->datatype}\\metadata";
                    $formfield = new $newfield($field->id, $instanceid);
                    if ($formfield->is_visible() && !$formfield->is_empty()) {
                        $output .= html_writer::tag('dt', format_string($formfield->field->name));
                        $output .= html_writer::tag('dd', $formfield->display_data());
                    }
                }
            }
        }
    }

    if (!$returnonly) {
        echo $output;
    } else {
        return $output;
    }
}

/**
 * Retrieves a list of profile fields that must be displayed in the sign-up form.
 * Specific to user profiles.
 *
 * @return array list of profile fields info
 * @since Moodle 3.2
 */
function local_metashared_get_signup_fields() {
    global $DB;

    $profilefields = [];
    // Only retrieve required custom fields (with category information)
    // results are sort by categories, then by fields.
    $sql = "SELECT uf.id as fieldid, ic.id as categoryid, ic.name as categoryname, uf.datatype
                FROM {local_metashared_field} uf
                JOIN {local_metashared_category} ic
                ON uf.categoryid = ic.id AND uf.signup = 1 AND uf.visible<>0
                WHERE uf.contextlevel = ?
                ORDER BY ic.sortorder ASC, uf.sortorder ASC";

    if ($fields = $DB->get_records_sql($sql, [CONTEXT_USER])) {
        foreach ($fields as $field) {
            $newfield = "\\metasharedfieldtype_{$field->datatype}\\metadata";
            $fieldobject = new $newfield($field->fieldid);

            $profilefields[] = (object)[
                'categoryid' => $field->categoryid,
                'categoryname' => $field->categoryname,
                'fieldid' => $field->fieldid,
                'datatype' => $field->datatype,
                'object' => $fieldobject
            ];
        }
    }
    return $profilefields;
}

/**
 * Adds code snippet to a moodle form object for custom profile fields that
 * should appear on the signup page
 * Specific to user profiles.
 * @param moodleform $mform moodle form object
 */
function local_metashared_signup_fields($mform) {

    if ($fields = local_metashared_get_signup_fields()) {
        foreach ($fields as $field) {
            // Check if we change the categories.
            if (!isset($currentcat) || $currentcat != $field->categoryid) {
                 $currentcat = $field->categoryid;
                 $mform->addElement('header', 'category_'.$field->categoryid, format_string($field->categoryname));
            };
            $field->object->edit_field($mform);
        }
    }
}

/**
 * Returns an object with the custom profile fields set for the given user
 * Specific to user profiles.
 * @param integer $instanceid
 * @param bool $onlyinuserobject True if you only want the ones in $USER.
 * @return stdClass
 */
function local_metashared_user_record($instanceid, $onlyinuserobject = true) {
    global $DB;

    $usercustomfields = new \stdClass();

    if ($fields = $DB->get_records('local_metashared_field', ['contextlevel' => CONTEXT_USER])) {
        foreach ($fields as $field) {
            $newfield = "\\metasharedfieldtype_{$field->datatype}\\metadata";
            $formfield = new $newfield($field->id, $instanceid);
            if (!$onlyinuserobject || $formfield->is_instance_object_data()) {
                $usercustomfields->{$field->shortname} = $formfield->data;
            }
        }
    }

    return $usercustomfields;
}

/**
 * Obtains a list of all available custom profile fields, indexed by id.
 *
 * Some profile fields are not included in the user object data (see
 * local_metashared_user_record function above). Optionally, you can obtain only those
 * fields that are included in the user object.
 *
 * To be clear, this function returns the available fields, and does not
 * return the field values for a particular user.
 *
 * @param bool $onlyinuserobject True if you only want the ones in $USER
 * @return array Array of field objects from database (indexed by id)
 * @since Moodle 2.7.1
 */
function local_metashared_get_custom_fields($contextlevel, $onlyinuserobject = false) {
    global $DB;

    // Get all the fields.
    $fields = $DB->get_records('local_metashared_field', ['contextlevel' => $contextlevel], 'id ASC');

    // If only doing the user object ones, unset the rest.
    if ($onlyinuserobject) {
        foreach ($fields as $id => $field) {
            $newfield = "\\metasharedfieldtype_{$field->datatype}\\metadata";
            $formfield = new $newfield();
            if (!$formfield->is_instance_object_data()) {
                unset($fields[$id]);
            }
        }
    }

    return $fields;
}

/**
 * Does the user have all required custom fields set?
 *
 * Internal, to be exclusively used by {@link user_not_fully_set_up()} only.
 *
 * Note that if users have no way to fill a required field via editing their
 * profiles (e.g. the field is not visible or it is locked), we still return true.
 * So this is actually checking if we should redirect the user to edit their
 * profile, rather than whether there is a value in the database.
 *
 * @param int $instanceid
 * @return bool
 */
function local_metashared_has_required_custom_fields_set($instanceid, $contextlevel) {
    global $DB;

    $sql = "SELECT f.id
              FROM {local_metashared_field} f
         LEFT JOIN {local_metashared} d ON (d.fieldid = f.id AND d.instanceid = ?)
             WHERE f.contextlevel = ? AND f.required = 1 AND f.visible > 0 AND f.locked = 0 AND d.id IS NULL";

    if ($DB->record_exists_sql($sql, [$instanceid, $contextlevel])) {
        return false;
    }

    return true;
}

/**
 * Returns the contextlevel defined for the specified context plugin name.
 *
 * @param int $contextlevel The context level to look for.
 * @return string The name of the located context.
 */
function local_metashared_get_contextname($contextlevel) {
    static $contextnames = []; // Cache for located contexts.

    $returnname = '';
    if (isset($contextnames[$contextlevel])) {
        $returnname = $contextnames[$contextlevel];
    } else {
        foreach (\local_metashared\context\context_handler::all_subplugins() as $contexthandler) {
            if ($contexthandler->contextlevel == $contextlevel) {
                $contextnames[$contextlevel] = $contexthandler->contextname;
                $returnname = $contexthandler->contextname;
                break;
            }
        }
    }
    return $returnname;
}

/**
 * Implements callback inplace_editable() allowing to edit values in-place
 *
 * @param string $itemtype
 * @param int $itemid
 * @param mixed $newvalue
 * @return local_metashared\output\inplace_editable
 */
function local_metashared_inplace_editable($itemtype, $itemid, $newvalue) {
    \external_api::validate_context(\context_system::instance());
    if ($itemtype === 'categoryname') {
        return local_metashared\output\categoryname::update($itemid, $newvalue);
    }
}

/**
 * Hook function that is called when settings blocks are being built. Call all context functions
 */
function local_metashared_extend_settings_navigation($settingsnav, $context) {
    global $PAGE, $DB, $USER, $CFG;
    //TODO: Make it Dynamic !
    $recorded_param_metashared = $DB->get_record("local_metashared_category",array("contextlevel" => 70));
    
    //--- get course of a categ ---//
    $hide = false;
    
    $syst = context_system::instance()->id;
    //if not admin on all the site 
  //  echo "==>".user_has_role_assignment($USER->id, 1, $syst);
   if (!user_has_role_assignment($USER->id, 1, $syst)) {
    foreach ($DB->get_records("course",array("category" => $recorded_param_metashared->idcateg)) as $c) {
        
        //Right for courses of the categ
        $access_admin = user_has_role_assignment($USER->id, 1, $DB->get_record('context', array('contextlevel' => CONTEXT_COURSE, 'instanceid' => $c->id))->id);
        $access_student = user_has_role_assignment($USER->id, 5, $DB->get_record('context', array('contextlevel' => CONTEXT_COURSE, 'instanceid' => $c->id))->id);
        $access_teacher = user_has_role_assignment($USER->id, 3, $DB->get_record('context', array('contextlevel' => CONTEXT_COURSE, 'instanceid' => $c->id))->id);
        $access_creator = user_has_role_assignment($USER->id, 2, $DB->get_record('context', array('contextlevel' => CONTEXT_COURSE, 'instanceid' => $c->id))->id);
        
        //echo $c->fullname."___".$access_admin."___".$access_student."___".user_has_role_assignment($USER->id, 1)."<br />";
        
        
        if (($access_student && !($access_admin || $access_teacher || $access_creator)) || (!$access_student && !($access_admin || $access_teacher || $access_creator))) { //if student but not admin on a course of the categ
         //   echo "true <br />";
            $hide = true;
        } elseif (!$access_student && ($access_admin || $access_teacher || $access_creator)) {//if not student but admin on a course of the categ
            $hide = false;
           // echo "false <br />";
            break;
        }
        
        
    }
   }
  //  die();
    //-------------//
 
    if ($recorded_param_metashared->idcateg != -1 && $recorded_param_metashared->idcateg != 0 
            && $recorded_param_metashared->hiddencateg == 1 && $hide) {
        //echo "amd call";
        $PAGE->requires->js_call_amd('local_metashared/confpage', 'hide_choosen_categ', array($recorded_param_metashared->idcateg));
        $PAGE->requires->js_call_amd('local_metashared/confpage', 'hide_breadcrumb', array($recorded_param_metashared->idcateg));
        
    }
    
        if ($DB->get_records('local_metashared_conf') && ($PAGE->pagelayout == "admin" || $PAGE->pagelayout == "incourse" || $PAGE->pagelayout=="course" || $PAGE->pagelayout=="coursecategory" || (isset($_GET["add"]) && isset($_GET["course"])))) {
            $lmc = array_values($DB->get_records('local_metashared_conf'));
            $in = false;
            $stop_check_categ = false;
            
            if ($PAGE->pagelayout == "admin") {
                if (substr($PAGE->pagetype,0,3) != "mod") {
                    $stop_check_categ = true;
                }
            }
            
            if (!$stop_check_categ) {
                foreach ($PAGE->categories as $k => $v) {
                    if ($k == $lmc[sizeof($lmc)-1]->working_theme_select) {
                        $in = true;
                    }
                }
            }
            
            if ($in) {
                //Correct ariane line and logo href 
                $PAGE->requires->css("/local/metashared/style/adapt_categ_style.css");
                $PAGE->requires->js_call_amd('local_metashared/confpage', 'change_logo_href', array($CFG->wwwroot, $lmc[sizeof($lmc)-1]->working_theme_select));
                
                //Add a link fo the advanced ressource research
                if ($PAGE->pagelayout=="coursecategory") {
                    $PAGE->requires->js_call_amd('local_metashared/confpage', 'add_link_to_advanced_research', array($CFG->wwwroot));
                }
                
                if ($PAGE->pagelayout== "incourse" && strpos($PAGE->url->get_path(false), "folder") != 0) {
                    //clean but slow
                    //$PAGE->requires->js_call_amd('local_metashared/confpage', 'hide_folder_button');
                    
                    //Dirty but quick
                    echo "<style> #next-activity-link, #prev-activity-link, #jump-to-activity {display : none;} </style>";
                }
            }
        }
    
    /*Stop Acces to the drop Menu, Can BE CUSTOM LATER
    foreach (\local_metashared\context\context_handler::all_enabled_subplugins() as $contexthandler) {
        $contexthandler->extend_settings_navigation($settingsnav, $context);
    }*/
    if ($PAGE->pagelayout == "course") {  
        //Add contextual Menu to add ressources in the "entrepot"
        $tmp = $DB->get_records('local_metashared_conf');
        if (in_array(array_shift ($tmp)->working_theme_select, array_keys ($PAGE->categories)) && !in_array($recorded_param_metashared->idcateg, array_keys ($PAGE->categories))) {
            /* looking for entrepot data */
            
            $list = array();
            $list[] = $recorded_param_metashared->idcateg;
            $categs = $DB->get_records("course_categories");
            foreach ($categs as $cs) {
               if (strpos($cs->path, $recorded_param_metashared->idcateg."/") !== false){
                   $list[] = $cs->id;
               }
            }
            $sql = "SELECT * FROM {course} WHERE category IN (";
            foreach ($list as $l) {
                $sql .= $l.",";
            }
            $sql = substr($sql, 0, -1);
            $sql .= ")";
            $courses = $DB->get_records_sql($sql);
          //  $courses = $DB->get_records("course",array("category" =>  $recorded_param_metashared->idcateg));
            
            $all_course_and_section = array();
            foreach ($courses as $c) {               
                $all_sections = array();
                foreach ($DB->get_records("course_sections",array("course" => $c->id)) as $s) {
                    if (!isset($s->name)) {
                        $name = "Section ".$s->section;
                    } else {
                        $name = $s->name;
                    }
                    $all_sections[] = ["id" => $s->id, "name" => $name, "section" => $s->section];
                }
                
                $all_course_and_section[] =["id" => $c->id, "name" => $c->fullname, "sections" => $all_sections];
            }
            /*launching JS for contexutal menu*/
            $PAGE->requires->js_call_amd('local_metashared/confpage', 'add_entrepot_copy_past_contextual_menu',array($all_course_and_section, sesskey(), $CFG->wwwroot));
            $PAGE->requires->js_call_amd('local_metashared/confpage', 'set_blank_in_links');
        }
        $tmp = $DB->get_records('local_metashared_conf');
        if (in_array(array_shift ($tmp)->working_theme_select, array_keys ($PAGE->categories)) && in_array($recorded_param_metashared->idcateg, array_keys ($PAGE->categories))) {
            if (isset($_GET['pastinentrepot'])) {
                if ($_GET['pastinentrepot'] == 1) {
                    $PAGE->requires->js_call_amd('local_metashared/custom_input_manager', 'init',array());
                    $mform = new metashared_form_copy_paste_entrepot($CFG->wwwroot . '/local/metashared/rec_new_module_metadata_copy_past_entrepot.php');
                    $mform->set_instance_id_field($_GET['instance']);
                    $mform->set_course_id_field($_GET['course']);
                    $mform->set_identifier_field($_GET['identifier']);
                    $mform->set_name_field($_GET['name_copy_past_entrepot']);
                    echo html_writer::div("", "fast_metashared", array("id" => "fast_metashared_mask")). html_writer::div($mform->render(), "fast_metashared", array("id" => "fast_metashared"));
                }
           }
        }
    }
}

/**
 * Hook function that is called when user profile page is being built. Call all context functions
 */
function local_metashared_myprofile_navigation(\core_user\output\myprofile\tree $tree, $user, $iscurrentuser, $course) {
    foreach (\local_metashared\context\context_handler::all_enabled_subplugins() as $contexthandler) {
        $contexthandler->myprofile_navigation($tree, $user, $iscurrentuser, $course);
    }
}

/**
 * Hook function to extend the course settings navigation. Call all context functions
 */
function local_metashared_extend_navigation_course($parentnode, $course, $context) {
    foreach (\local_metashared\context\context_handler::all_enabled_subplugins() as $contexthandler) {
        $contexthandler->extend_navigation_course($parentnode, $course, $context);
    }
}

/**
 * This function extends the navigation with the metadata for user settings node.
 *
 * @param navigation_node $navigation  The navigation node to extend
 * @param stdClass        $user        The user object
 * @param context         $usercontext The context of the user
 * @param stdClass        $course      The course to object for the tool
 * @param context         $coursecontext     The context of the course
 */
function local_metashared_extend_navigation_user_settings($navigation, $user, $usercontext, $course, $coursecontext) {
  /*  foreach (\local_metashared\context\context_handler::all_enabled_subplugins() as $contexthandler) {
        $contexthandler->extend_navigation_user_settings($navigation, $user, $usercontext, $course, $coursecontext);
    }*/
}

/**
 * Hook function to insert metadata form elements in the native module form
 * @param moodleform $formwrapper The moodle quickforms wrapper object.
 * @param MoodleQuickForm $mform The actual form object (required to modify the form).
 */
function local_metashared_coursemodule_standard_elements($formwrapper, $mform) {
    global $COURSE, $DB, $PAGE;
    $COURSE->category;
    $COURSE->id;
    $PAGE->requires->js_call_amd('local_metashared/custom_input_manager', 'init',array());
    //TODO : Make it dynamic !
    $recorded_param_metashared = $DB->get_record("local_metashared_category",array("contextlevel" => 70/*$GLOBALS['context']->contextlevel*/ ));
    
    $sub_categ = explode("/",$DB->get_record("course_categories", array("id" => $COURSE->category))->path);
    
    $sub_categ_bool = false;
    foreach ($sub_categ as $sc) {
        if ($sc == $recorded_param_metashared->idcateg) {
            $sub_categ_bool= true;
        }
    }
    
    if (
        ($sub_categ_bool || $recorded_param_metashared->idcateg == $COURSE->category || $recorded_param_metashared->idcateg == 0 || $recorded_param_metashared->idcateg == NULL)
        && 
        ($recorded_param_metashared->idcourse == $COURSE->id  || $recorded_param_metashared->idcourse == 0 || $recorded_param_metashared->idcourse == NULL)
        &&
        $recorded_param_metashared->idcateg != -1
        &&
        $recorded_param_metashared->idcourse != -1) {
        $PAGE->requires->css('/local/metashared/style/adapt_edit_panel.css');
        foreach (\local_metashared\context\context_handler::all_enabled_subplugins() as $contexthandler) {
            $contexthandler->coursemodule_standard_elements($formwrapper, $mform);
        }
    }
}

/**
 * Hook the add/edit of the course module.
 *
 * @param stdClass $data Data from the form submission.
 * @param stdClass $course The course.
 */
function local_metashared_coursemodule_edit_post_actions($data, $course) {
    global $DB, $CFG, $USER, $COURSE;
    foreach (\local_metashared\context\context_handler::all_enabled_subplugins() as $contexthandler) {
        $data = $contexthandler->coursemodule_edit_post_actions($data, $course);
    }
    $add = null;
    if(isset($_GET["add"])) {
       $add = "metasharedrc";
    } elseif($data->add) {
        $add = $data->add;
    }
    
    if($add != "metasharedrc") {
            //TODO : Make it dynamic !
       $recorded_param_metashared = $DB->get_record("local_metashared_category",array("contextlevel" => 70/*$GLOBALS['context']->contextlevel*/ ));

       if (
           ($recorded_param_metashared->idcateg == $COURSE->category || $recorded_param_metashared->idcateg == 0 || $recorded_param_metashared->idcateg == NULL)
           && 
           ($recorded_param_metashared->idcourse == $COURSE->id  || $recorded_param_metashared->idcourse == 0 || $recorded_param_metashared->idcourse == NULL)
           &&
           $recorded_param_metashared->idcateg != -1
           &&
           $recorded_param_metashared->idcourse != -1) {
           //Dangerous approch, need to be fixed
               $url_metashared_stuff = $CFG->wwwroot."/mod/$data->modulename/view.php?id=$data->coursemodule";

               $shentry = new StdClass;
                   $shentry->title = $data->name;
                   $shentry->type = $data->add;
                   $shentry->mimetype =  mimeinfo('type', $url_metashared_stuff);
                   $shentry->identifier = sha1($url_metashared_stuff);
                   $shentry->remoteid = '';
                   $shentry->file = "none";
                   $shentry->url = $url_metashared_stuff;
                   $shentry->lang = $USER->lang; // not more used
                   $shentry->description = "description de ".$data->name;
                   $shentry->keywords = "none";
                   $shentry->timemodified = time();
                   $shentry->provider = 'local';
                   $shentry->isvalid = 1;
                   $shentry->context = 1;
                   $shentry->scoreview = 0;
                   $shentry->scorelike = 0;
                   $shentry->id_instance = $data->coursemodule;
               if (empty($DB->get_record('metasharedrc_entry', array("id_instance" => $data->coursemodule)))){
                   $DB->insert_record('metasharedrc_entry', $shentry);
               }
           }
    }
    //$data->
    //construction de l'url pour la sauvegarde "$CFG->wwwroot/mod/$data->modulename/view.php?id=$data->coursemodule")
    return $data;
}