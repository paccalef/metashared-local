<?php
    require_once(dirname(__FILE__) . '/../../config.php');
    global $DB;
    $module_info = $DB->get_record_sql("SELECT m.id, m.name FROM {course_modules} cm INNER JOIN {modules} m ON cm.module = m.id WHERE cm.id=".$_POST['cmid'], array());
    $module_name = $module_info->name;
    $course_str = "";
    
    $list = array();
    $list[] = $DB->get_record("local_metashared_category",array())->idcateg;
    $categs = $DB->get_records("course_categories");
    foreach ($categs as $cs) {
        if (strpos($cs->path, $DB->get_record("local_metashared_category",array())->idcateg."/") !== false){
            $list[] = $cs->id;
        }
    }
    $sql = "SELECT id FROM {course} WHERE category IN (";
    foreach ($list as $l) {
        $sql .= $l.",";
    }
    $sql = substr($sql, 0, -1);
    $sql .= ")";
    $coursesss = $DB->get_records_sql($sql);
    
    foreach (array_keys($coursesss) as $key) {
        $course_str .= $key.",";
    }
    
    $course_str = substr($course_str, 0, -1);
    $sql = "SELECT m.id FROM {course_modules} cm INNER JOIN {".$module_name."} m ON m.id = cm.instance WHERE cm.COURSE IN(".$course_str.") AND cm.module=".$module_info->id." AND m.name='".$_POST['name']."' AND cm.deletioninprogress=0 AND cm.idnumber IS NOT NULL";
    $allready = false;
    if (sizeof($DB->get_records_sql($sql)) > 0) {
        $allready = true;
    }
    
    //if already looking for a "copie" name
    if ($allready) {
        $al_cop_name = true;
        $name = $_POST['name']." - COPIE";
        while ($al_cop_name) {
            
            $sql = "SELECT m.id FROM {course_modules} cm INNER JOIN {".$module_name."} m ON m.id = cm.instance WHERE cm.COURSE IN(".$course_str.") AND cm.module=".$module_info->id." AND m.name='".$name."' AND cm.deletioninprogress=0 AND cm.idnumber IS NOT NULL";
            if (!(sizeof($DB->get_records_sql($sql)) > 0)) {
                $al_cop_name = false;
            } else {
                $name = $name." - COPIE";
            }
        }
        
    }
    
    header("Content-Type: application/json", true);
    if ($allready) {
        echo json_encode($name);
    } else {
        echo json_encode("");
    }
    