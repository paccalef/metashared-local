<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Menu profile field.
 *
 * @package    profilefield_menu
 * @copyright  2007 onwards Shane Elliot {@link http://pukunui.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace metasharedfieldtype_menu;

defined('MOODLE_INTERNAL') || die;

/**
 * Class local_metashared_field_menu
 *
 * @copyright  2007 onwards Shane Elliot {@link http://pukunui.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class metadata extends \local_metashared\fieldtype\metadata {

    /** @var array $options */
    public $options;

    /** @var int $datakey */
    public $datakey;

    /**
     * Constructor method.
     *
     * Pulls out the options for the menu from the database and sets the the corresponding key for the data if it exists.
     *
     * @param int $fieldid
     * @param int $instanceid
     * @param object $fielddata optional data for the field object.
     */
    public function __construct($fieldid = 0, $instanceid = 0, $fielddata = null) {
        // First call parent constructor.
        parent::__construct($fieldid, $instanceid, $fielddata);

        // Param 1 for menu type is the options.
        if (isset($this->field->param1)) {
            $options = explode("\n", $this->field->param1);
        } else {
            $options = [];
        }
        $this->options = [];
        if (!empty($this->field->required)) {
            $this->options[''] = get_string('choose').'...';
        }
        foreach ($options as $key => $option) {
            $this->options[$option] = format_string($option); // Multilang formatting with filters.
        }

        // Set the data key.
        if ($this->data !== null) {
            $key = $this->data;
            if (isset($this->options[$key]) || ($key = array_search($key, $this->options)) !== false) {
                $this->data = $key;
                $this->datakey = $key;
            }
        }
    }

    /**
     * Create the code snippet for this field instance
     * Overwrites the base class method
     * @param moodleform $mform Moodle form instance
     */
    public function edit_field_add($mform) {
        global $PAGE;
     //  $mform->addElement('select', $this->inputname, format_string($this->field->name), $this->options);
       
        $options_item = array();
        $options_categ = array();
       // $data_from_categ = array(); 
     //   $already_selected = false;
        $i = 0;
        foreach (explode("\n", $this->field->param1) as $op) {
            if (substr($op, 0, 2)  == "--" && $options_item == array()) {
                $title_categ = substr($op, 2);
                $options_categ[] = '<option value="'.$title_categ.'">'.$title_categ.'</option>';
                $options_item[$i] .= '<select style="display:none;" class="'.$this->inputname.' '.str_replace(" ","",$title_categ).' custom-select clone-input '.substr($op,2,sizeof($op)).'" name="'.$this->inputname.'_clone" id="id_'.$this->inputname.'_clone">';
                $options_item[$i] .= '<option></option>';
                
            } elseif (substr($op, 0, 2)  == "--") {
                $title_categ = substr($op, 2);
                $options_categ[] = '<option value="'.$title_categ.'">'.$title_categ.'</option>';
                $options_item[$i] .= '</select>';
                $i++;
                $options_item[$i] .= '<select style="display:none;" class="'.$this->inputname.' '.str_replace(" ","",$title_categ).' custom-select clone-input '.substr($op,2,sizeof($op)).'" name="'.$this->inputname.'_clone" id="id_'.$this->inputname.'_clone">';
                $options_item[$i] .= '<option></option>'; 
            } elseif (substr($op, 0, 2)  !== "--" && $options_item == array())  {
                $mform->addElement('select', $this->inputname, format_string($this->field->name), $this->options);
                break;
            } else {
          /*       $selected = "";
                if ($this->data == $op && $already_selected == false) {*/
                  // $already_selected = true;
                //   $selected = "selected";
        //           $options_item[$i] = str_replace('<select style="display:none;"', '<select', $options_item[$i]);
               //    $options_categ[sizeof($options_categ)-1] = str_replace('<option', '<option selected', $options_categ[sizeof($options_categ)-1]);
            //    }
                   
               // $data_from_categ[str_replace(" ","",$title_categ)] [] = $op;
                $options_item[$i] .= '<option '/*.$selected*/.' value="'.trim($op).'">'.trim($op).'</option>';
            }
        }
        
        if ($options_categ != array()) {
            $options_item[$i] .= '</select>';

            $result = '';
            foreach($options_item as $key => $oi) {
                if ($key == 0) {
                    $oi = str_replace('<select style="display:none;"', '<select', $oi);
                }
                $result .= $oi;
            }

            $result_categ = '';
            foreach($options_categ as $oc) {
                $result_categ .= $oc;
            }
            
            
            $result_pre_data = "";
            $result_pre_data_raw = array();
            foreach (explode("\n", $this->data) as $da) {
               $da = trim(str_replace("\n","",$da));
               $result_pre_data .= '<span field="'.$this->inputname.'" id="'.$da.'" class="selected_m_item tag tag-info mb-3 mr-1" style="cursor: pointer;"><span aria-hidden="true">× </span>'.$da.'</span>';
               $result_pre_data_raw[] = $da;
               
            }
            
            if ($result_pre_data == "") {
                $result_pre_data .='<i>&nbsp;&nbsp;Click on items to add them </i>';
            }

            $PAGE->requires->js_call_amd('local_metashared/custom_input_manager', 'set_data',array($this->inputname,$result_pre_data_raw));

            $mform->addElement('hidden', $this->inputname,$this->data);
            $mform->addElement('html', '<div class="form-group row  fitem   ">'
                                       .'<div class="col-md-3">'.$this->field->name.'</div>'
                                        . '<div class="col-md-9 form-inline felement">'
                                           . '<table id="'.$this->inputname.'_table">'
                                            . '<tr id="first_line">'
                                               . '<td>'
                                                  . '<select class="custom-select categ-selector" id="'.$this->inputname.'">'
                                                   . $result_categ
                                                  . '</select>'
                                                . '</td>'
                                                . '<td>'
                                                . $result
                                                . '</td>'
                                               /* . '<td>'
                                                    . '<span class="add_custom_select_line" field="'.$this->inputname.'">&nbsp;Add Values</span>'
                                                . '</td>'*/
                                            . '</tr>'
                                            . '<tr>'
                                                .'<td colspan="3" id="selected-item-'.$this->inputname.'">'
                                                . $result_pre_data
                                                .'</td>'
                                            . '</tr>'
                                              . '</table>'
                                         . '</div>'
                                        .'</div>');
        }
    }

    /**
     * Set the default value for this field instance
     * Overwrites the base class method.
     * @param moodleform $mform Moodle form instance
     */
    public function edit_field_set_default($mform) {
        $key = $this->field->defaultdata;
        if (isset($this->options[$key]) || ($key = array_search($key, $this->options)) !== false) {
            $defaultkey = $key;
        } else {
            $defaultkey = '';
        }
        $mform->setDefault($this->inputname, $defaultkey);
    }

    /**
     * The data from the form returns the key.
     *
     * This should be converted to the respective option string to be saved in database
     * Overwrites base class accessor method.
     *
     * @param mixed $data The key returned from the select input in the form
     * @param stdClass $datarecord The object that will be used to save the record
     * @return mixed Data or null
     */
    public function edit_save_data_preprocess($data, $datarecord) {
       // return isset($this->options[$data]) ? $data : null;
        return $data;
    }

    /**
     * When passing the instance object to the form class for the edit page
     * we should load the key for the saved data
     *
     * Overwrites the base class method.
     *
     * @param stdClass $instance Instance object.
     */
    public function edit_load_instance_data($instance) {
        $instance->{$this->inputname} = $this->datakey;
    }

    /**
     * HardFreeze the field if locked.
     * @param moodleform $mform instance of the moodleform class
     */
    public function edit_field_set_locked($mform) {
        if (!$mform->elementExists($this->inputname)) {
            return;
        }
        if ($this->is_locked() && !has_capability('moodle/user:update', context_system::instance())) {
            $mform->hardFreeze($this->inputname);
            $mform->setConstant($this->inputname, format_string($this->datakey));
        }
    }
    /**
     * Convert external data (csv file) from value to key for processing later by edit_save_data_preprocess
     *
     * @param string $value one of the values in menu options.
     * @return int options key for the menu
     */
    public function convert_external_data($value) {
        if (isset($this->options[$value])) {
            $retval = $value;
        } else {
            $retval = array_search($value, $this->options);
        }

        // If value is not found in options then return null, so that it can be handled
        // later by edit_save_data_preprocess.
        if ($retval === false) {
            $retval = null;
        }
        return $retval;
    }

    /**
     * Return the field type and null properties.
     * This will be used for validating the data submitted by a user.
     *
     * @return array the param type and null property
     * @since Moodle 3.2
     */
    public function get_field_properties() {
        return [PARAM_TEXT, NULL_NOT_ALLOWED];
    }
}


