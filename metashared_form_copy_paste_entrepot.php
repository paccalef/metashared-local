<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/metashared/lib.php');
require_once($CFG->dirroot.'/course/moodleform_mod.php');

class metashared_form_copy_paste_entrepot extends moodleform {

   function definition() {
        global $CFG, $DB;
        $mform = $this->_form;
        local_metashared_definition($mform, 0, 70);
        $mform->addElement('submit',  'btnsave', "ok");
    } 
    
    function set_instance_id_field($id) {
        $this->_form->addElement('hidden', 'instance_id', $id);
    }
    
    function set_identifier_field($id) {
        $this->_form->addElement('hidden', 'identifier', $id);
    }
    
    function set_course_id_field($id) {
        $this->_form->addElement('hidden', 'course_id', $id);
    }
    
    function set_name_field($id) {
        $this->_form->addElement('hidden', 'name', $id);
    }
}