<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package local_metashared
 * @author Mike Churchward <mike.churchward@poetopensource.org>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @copyright 2017, onwards Poet
 */

defined('MOODLE_INTERNAL') || die;

// Required for non-standard context constants definition.
require_once($CFG->dirroot.'/local/metashared/lib.php');

if ($hassiteconfig) {
    $ADMIN->add('localplugins', new admin_category('metasharedfolder', get_string('metashared', 'local_metashared')));
    $contextplugins = core_component::get_plugin_list('metasharedcontext');

    // Create a settings page and add an enable setting for each metadata context type.
    $settings = new admin_settingpage('local_metashared', get_string('settings'));
    if ($ADMIN->fulltree) {
        foreach ($contextplugins as $contextname => $contextlocation) {
            $item = new admin_setting_configcheckbox('metasharedcontext_'.$contextname.'/metasharedenabled',
                new lang_string('metasharedenabled', 'metasharedcontext_'.$contextname), '', 0);
            if ($contextname == "module")
            $settings->add($item);
        }
    }
    $ADMIN->add('metasharedfolder', $settings);

    // Create a new external settings page for each metadata context type data definitions.
    foreach ($contextplugins as $contextname => $contextlocation) {
        $contexthandler = \local_metashared\context\context_handler::factory($contextname);
      
        if ($contextname == "module")
        $ADMIN->add('metasharedfolder',
            new admin_externalpage('metasharedcontext_'.$contextname, get_string('metasharedtitle', 'metasharedcontext_'.$contextname),
                new moodle_url('/local/metashared/index.php', ['contextlevel' => $contexthandler->contextlevel]),
                    ['moodle/site:config']));

        // Add context settings to specific context settings pages (if possible).
        if (get_config('metasharedcontext_'.$contextname, 'metasharedenabled') == 1) {
            $contexthandler->add_settings_to_context_menu($ADMIN);
        }
    }

    $settings = null;
}