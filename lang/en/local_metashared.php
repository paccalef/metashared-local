<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package local_metashared
 * @author Mike Churchward <mike.churchward@poetopensource.org>
 * @copyright 2017 onwards Mike Churchward (mike.churchward@poetopensource.org)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Metadata Shared';

$string['errorcontextnotfound'] = 'Invalid context subplugin "{$a->contextname}" requested.';
$string['instancemetashared'] = '{$a->instancename} metashared';
$string['metashared'] = 'Metadata Shared';
$string['metashareddata'] = 'Value';
$string['metashareddescription'] = 'Description';
$string['metasharedfor'] = 'Instance metashared';
$string['metasharedname'] = 'Name';
$string['metasharedsaved'] = 'Metadata saved.';
$string['privacy:metashared'] = 'Metadata plugin only manages data and fields. Individual subplugins may store personal data.';
$string['subplugintype_metasharedcontext'] = 'Data context plugin';
$string['subplugintype_metasharedcontext_plural'] = 'Data context plugins';
$string['subplugintype_metasharedfieldtype'] = 'Data fieldtype plugin';
$string['subplugintype_metasharedfieldtype_plural'] = 'Data fieldtype plugins';
