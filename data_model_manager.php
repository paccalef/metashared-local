<?php

/* 
    ALLOW TO INSTALL ALL THE METADATA FOR A SELECT ONE ... With all the area .. etc.. 
 */
namespace mod_metasharedrc;
require_once(dirname(__FILE__) . '/../../config.php');
//local_metashared_edit_field($id, $datatype, $redirect, $contextlevel);
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->dirroot.'/local/metashared/lib.php');
require_once($CFG->dirroot.'/local/metashared/definelib.php');
require_once($CFG->dirroot.'/mod/metasharedrc/plugins/suplomfr/plugin.class.php');
require_once($CFG->dirroot.'/mod/metasharedrc/metadatalib.php');

class data_model_manager
{

  public function install_model($description) {
      
      //metasharedrc_get_plugin /var/www/html/moodle/mod/metasharedrc/plugins/suplomfr/plugin.class.php
      foreach ($description as $d) {
          if ($d["type"] != "menu") {
            $this->add_one_field($d["category"],$d["name"], $d["type"]);
          } else {
              $this->add_one_field($d["category"],$d["name"], $d["type"], $d["options"]);
          }
      }
  }
  
  private function add_one_field($cat, $name, $type, $options = null ) {
    GLOBAL $DB;

    $context_level = 70;
    $ident_name = metadata_local_clean_string_key(strtolower($cat[0]."_".$name));
    $datatype = $type;
    
    $data = (object)array(
        'id'=> NULL, 'contextlevel'=>$context_level, 'shortname'=>$ident_name, 'name' => $name, 'datatype' => $datatype,'description' => "", 
        'descriptionformat' => 1, 'categoryid'=> 1, 'sortorder' => 1, 'required' =>0, 'visible' =>2, 'forceunique' => 0, 'signup' => 0, 'defaultdataformat' =>0 , 'param1' => 100,'param2' => 2048, 'param3' => 0);
    
    if($type == "menu" && $options != null) {
        $all_options = "";
        foreach (array_values($options) as $k=>$o) {
            if ($o <="100" && $o >="0") {
                $value = $o;
            } else {
                $value = get_string(metadata_local_clean_string_key($o), 'sharedmetadata_suplomfr');
            }
            if (($k+1) < sizeof(array_values($options))) {
                $all_options = $all_options.$value."\n";
            } else {
               $all_options = $all_options.$value;
            }
        }
        
        $i=1;
        $data->{"param".$i} = $all_options;
    }
    
    $DB->insert_record('local_metashared_field', $data);
  }
  
  public function remove_model() {
    echo "remove all metadata";
  }
  
  public function trad_type($str) {
    $type = "text";
    if ($str == "codetext" || $str == "text") {
        $type = "text"; 
    } elseif ($str == "select") {
        $type = "menu";
    } elseif  ($str == "date") {
        $type = "datetime";
    } elseif ($str == "vcard") {
        $type = "textarea";
    } 
    
    return $type;
  }
  
  public function install_model_suplomfr() {
    $plugin_suplomfr = new plugin_suplomfr();
    
    $data = $plugin_suplomfr->METADATATREE;

    $description = array();
    // for each elem of the tree
    foreach ($data as $key => $elem) {
        if ($elem["type"] == "category") {
            $description[] = array("category" => array($elem["name"]),"name" => get_string(metadata_local_clean_string_key(strtolower($elem["name"])), 'sharedmetadata_suplomfr'),"type" => "title");
            foreach ($elem["childs"] as $k => $e) {
                if ($data[$k]["type"] === "category") {
                  foreach ($data[$k]["childs"] as $kk => $el) {
                    $type = $this->trad_type($data[$kk]["type"]);
                    if ($type == "menu") {
                        $description[] = array("category" => array($elem["name"],$data[$kk]["name"]),"name" => get_string(metadata_local_clean_string_key(strtolower($data[$kk]["name"])), 'sharedmetadata_suplomfr'),"type" => $type, "options" => $data[$kk]["values"]);
                    } elseif ($type != "category") {
                        $description[] = array("category" => array($elem["name"],$data[$kk]["name"]),"name" => get_string(metadata_local_clean_string_key(strtolower($data[$kk]["name"])), 'sharedmetadata_suplomfr'),"type" => $type);
                    }
                  }
                } else {
                    $type = $this->trad_type($data[$k]["type"]);
                    if ($type == "menu") {
                        $description[] = array("category" => array($elem["name"]),"name" => get_string(metadata_local_clean_string_key(strtolower($data[$k]["name"])), 'sharedmetadata_suplomfr'),"type" => $type,"options" => $data[$k]["values"]);
                    } elseif ($type != "category") {
                        $description[] = array("category" => array($elem["name"]),"name" => get_string(metadata_local_clean_string_key(strtolower($data[$k]["name"])), 'sharedmetadata_suplomfr'),"type" => $type);
                    }
                    
                }
                unset($data[$k]);
            }
            unset($data[$key]);
        }
        
    }
    
    return $description;
  }
}


/*
  $options[$value] = preg_replace('/\[\[|\]\]/', '', get_string(str_replace(' ', '_', strtolower($value)), 'sharedmetadata_'.$this->namespace));
            }
            $mform->addElement($element['type'], $element['name'], get_string(metadata_local_clean_string_key($element['name']), 'sharedmetadata_'.suplomfr), $options);
        } else {
            $mform->addElement($element['type'], $element['name'], get_string(metadata_local_clean_string_key($element['name']), 'sharedmetadata_'.$this->namespace));
 */
$data_model_manager = new data_model_manager();
$description = $data_model_manager->install_model_suplomfr();
$data_model_manager->install_model($description);

echo "Modèle créé ! Faites précédent et actualisez la page !";