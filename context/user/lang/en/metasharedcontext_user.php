<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Metadata user context plugin language file.
 *
 * @package local_metashared
 * @subpackage metasharedcontext_user
 * @author Mike Churchward <mike.churchward@poetopensource.org>
 * @copyright 2017 onwards Mike Churchward (mike.churchward@poetopensource.org)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$string['contextname'] = 'user';
$string['contexttitle'] = 'User';
$string['metasharedtitle'] = 'User metashared';
$string['metashareddisabled'] = 'Metadata for users is currently disabled.';
$string['metasharedenabled'] = 'Use metashared for users';
$string['pluginname'] = 'User metashared context';
$string['privacy:metashared:data'] = 'The data applied to this field for this user.';
$string['privacy:metashared:fielddescription'] = 'The textual description of the field.';
$string['privacy:metashared:fieldid'] = 'The ID of the field describing this data';
$string['privacy:metashared:fieldname'] = 'The name of the field containing the data';
$string['privacy:metashared:local_metashared'] = 'The table that stores the user data.';
$string['privacy:metashared:local_metashared_field'] = 'The table that stores the field descriptors.';
$string['privacy:metashared:userid'] = 'The ID of the user this data applies to.';