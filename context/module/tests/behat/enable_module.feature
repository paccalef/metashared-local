@local @local_metashared @metasharedcontext @metasharedcontext_module
Feature: Enable module context plugin
  In order to use metashared for modules
  As an admin
  I need to enable metashared for modules

  @javascript
  Scenario: Enable metashared for modules
    Given the following "users" exist:
      | username | firstname | lastname | email |
      | teacher1 | Teacher | 1 | teacher1@example.com |
    And the following "courses" exist:
      | fullname | shortname | category |
      | Course 1 | C1 | 0 |
    And the following "activities" exist:
      | activity   | name                   | intro                         | course | idnumber     | groupmode |
      | forum      | Standard forum name    | Standard forum description    | C1     | forum1       | 0         |
    And the following "course enrolments" exist:
      | user | course | role |
      | teacher1 | C1 | editingteacher |
    And I log in as "admin"
    And I navigate to "Settings" node in "Site administration > Plugins > Local plugins > Metashared"
    And I set the field "id_s_metasharedcontext_module_metasharedenabled" to "1"
    And I press "Save changes"
    Then the field "s_metasharedcontext_module_metasharedenabled" matches value "1"

    And I navigate to "Plugins" node in "Site administration"
    Then I should see "Module metashared"
    And I navigate to "Module metashared" node in "Site administration > Plugins"
    Then I should see "Module metashared"
    And I should see "Create a new profile field:"
    And I should see "Create a new profile category"
    And I set the field "datatype" to "menu"
    Then I should see "Creating a new 'Dropdown menu' profile field"
    And I set the field "id_shortname" to "subjectmatter"
    And I set the field "id_name" to "Subject matter"
    And I set the field "id_param1" to multiline:
    """
    Languages
    Arts
    Sciences
    Mathematics
    History
    Social Studies
    Other
    """
    And I set the field "id_defaultdata" to "Other"
    And I press "Save changes"
    Then I should see "Module metashared"
    And I should see "Subject matter"

    And I am on "Course 1" course homepage
    And I follow "Standard forum name"
    And I navigate to "Module metashared" in current page administration
    Then I should see "Subject matter"
    And I set the field "id_local_metashared_field_subjectmatter" to "History"
    And I press "Save changes"
    And I should see "Metashared saved"
    And I press "Cancel"
    And I navigate to "Module metashared" in current page administration
    Then I should see "Subject matter"
    And the field "id_local_metashared_field_subjectmatter" matches value "History"

    And I am on "Course 1" course homepage
    And I follow "Standard forum name"
    And I navigate to "Edit settings" in current page administration
    Then I should see "Other fields"
    And I click on "Other fields" "link"
    Then I should see "Subject matter"
    And the field "id_local_metashared_field_subjectmatter" matches value "History"
