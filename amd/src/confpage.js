define(['jquery'], function ($) {
    var confpage = {};
    var all_course_and_section = [];

    confpage.set_blank_in_links =function() {
        $("a").each(function() {
            if ($(this)[0].href.includes("metasharedrc")) {
                $(this).attr("target","_blank");
            }
        });
    };

    confpage.redirect_record_shared_resource = function(wwwroot, cm, course_id, sesskey, section, name, old_name) {
        $.ajax({
            url: wwwroot+"/local/metashared/is_already_this_mod.php",
            method: "POST",
            data: { cmid : cm, name : name },
            dataType: "json",
            success: function( response ) {
                if (response === "") {
                    if (old_name === "") {
                            document.location.href = wwwroot+"/local/metasharedrcs/record_shared_resource_from_existing_elem.php?"+
                                "cmid="+cm+"&courseid="+course_id+"&sesskey="+sesskey+"&section="+section+"&copy=1"
                                +"&name="+name+"&pastinentrepot=1'";
                        } else {
                            document.location.href = wwwroot+"/local/metasharedrcs/record_shared_resource_from_existing_elem.php?"+
                                "cmid="+cm+"&courseid="+course_id+"&sesskey="+sesskey+"&section="+section+"&copy=1"
                                +"&name="+name+"&pastinentrepot=1&old_name="+old_name;
                        }
                } else {
                    var valeur = prompt("Cette ressource semble déjà être dans l'entrepot,"+
                            "Saisisser un nouveau nom ou annuler.", response);

                    if (valeur !== null) {
                        if (old_name === "") {
                            confpage.redirect_record_shared_resource(wwwroot, cm, course_id, sesskey, section, valeur, name);
                        } else {
                            confpage.redirect_record_shared_resource(wwwroot, cm, course_id, sesskey, section, valeur, old_name);
                        }
                    }
                }
            },
            error: function( error )
            {
                alert( "Erreur "+error );
            }
        });
    };

    confpage.pop_entrepot_copy_past = function(cm, sesskey, wwwroot,name) {
        var tab ="<ul>";
        all_course_and_section.forEach(function(course){
            tab += "<li id_course='"+course.id+"' class='li_entrepot_copy_past'>"+course.name+"</li>";
            tab += "<ol id_course='"+course.id+"' class='ol_entrepot_copy_past ol_course_section_"+course.id+"'>";
            course.sections.forEach(function(section){

            tab += "<li id_section='"+section.id+"' id_course='"+course.id+"' "+
                        "class='li_section_entrepot_copy_past'>"+
                        "<a href='javascript:confpage.redirect_record_shared_resource(\""+
                        wwwroot+"\","+cm+","+course.id+",\""+sesskey+
                        "\","+section.section+",\""+name+"\",\"\");'>"+section.name+"</a></li>";
            });
/*
                tab += "<li id_section='"+section.id+"' id_course='"+course.id+"' "+
                        "class='li_section_entrepot_copy_past'><a href='"+
                        wwwroot+"/local/metasharedrcs/record_shared_resource_from_existing_elem.php?"+
                        "cmid="+cm+"&courseid="+course.id+"&sesskey="+sesskey+"&section="+section.section+"&copy=1"
                        +"&name="+name+"&pastinentrepot=1'>"+section.name+"</a></li>";
            });*/
            tab += "</ol>";
        });
        tab +="</ul>";

       $('body').append("<div id='shadow_fancy_background'></div>"+
               "<div id='entrepot_fancy'>"+
               "<div id='sub_contener_entrepot_fancy'>"+
               "<h2>Copier vers l'entrepot</h2>"+
                tab+
               "</div></div>");

        $( "#shadow_fancy_background" ).click(function() {
            $( "#shadow_fancy_background" ).remove();
            $('#entrepot_fancy').remove();
        });

        $('.li_entrepot_copy_past').click(function() {
            if ($(".ol_course_section_"+$(this).attr('id_course')).is(":hidden")) {
                $(".ol_course_section_"+$(this).attr('id_course')).show();
            } else {
                $(".ol_course_section_"+$(this).attr('id_course')).hide();
            }
        });
    };

    confpage.add_entrepot_copy_past_contextual_menu = function(data,sesskey,wwwroot) {
        all_course_and_section = data;
        $( ".dropdown-toggle" ).click(function() {
            var rsc_id = $("#"+$(this).parents()[2].id).attr("data-owner").replace("#module-", "");
            var drop_menu = $("#"+$('[data-owner="#module-' + rsc_id + '"]').children()[0].children[0].id+" > .dropdown-menu");
            var name = $("#"+$('[data-owner="#module-' + rsc_id + '"]')
                    .parent().parent()[0].id).children().eq(0).children().eq(0).attr("data-value");
            if (!drop_menu.html().includes("Copier vers Entrerpôt")) {
                drop_menu.html(drop_menu.html()+
                "<span onclick=\"confpage.pop_entrepot_copy_past("+rsc_id+",'"+sesskey+"','"+wwwroot+"','"+name+"');"+
                "return false;\" "+
                'class="dropdown-item menu-action cm-edit-action add-rsc-entrepot" role="menuitem">' +
                '<span class="menu-action-text"> Copier vers Entrerpôt</span></span>');
            }
        });
    };

    confpage.hide_choosen_categ = function(id) {
        $( "div[data-categoryid='"+id ).hide();
        /*
           var go_hide = false;
           var i = 0;
           jQuery(".breadcrumb-item").each(function(){ 
            if (!go_hide) {
                if(jQuery(this).html().includes("course/index.php?categoryid=12")) {
                    go_hide = true;
                    jQuery(this).hide();
                }
            } else {
                if (i < jQuery(".breadcrumb-item").length-1)
                jQuery(this).hide();
            }
            i++;
          });
         */
    };

    confpage.hide_folder_button = function () {
        $("#next-activity-link").hide();
        $("#prev-activity-link").hide();
        $("#jump-to-activity").hide();
    };

    confpage.hide_breadcrumb = function(id) {
         var go_hide = false;
           var i = 0;
           $(".breadcrumb-item").each(function(){
            if (!go_hide) {
                if($(this).html().includes("course/index.php?categoryid="+id)) {
                    go_hide = true;
                    $(this).hide();
                }
            } else {
                if (i < $(".breadcrumb-item").length-1) {
                    $(this).hide();
                }
            }
            i++;
          });
    };
    confpage.change_logo_href = function(wwwroot, id) {
        $(".has-logo").attr("href",wwwroot+"/course/index.php?categoryid="+id);
    };
    confpage.add_link_to_advanced_research = function(wwwroot) {
        $("#coursesearch").after("<div class='categorypicker'>"+
        "<button> <a href='"+wwwroot+"/mod/metasharedrc/search.php?linked=1'>"+
        "Accès Librairie Mutualisée</a></button></div>");
    };
    confpage.rec_new_categ_filter = function(value, name, contextlevel) {
        $.ajax({
            url: "categ_filter.php",
            method: "POST",
            data: { valueinput : value, nameinput : name,contextlevel : contextlevel },
            dataType: "json",
            success: function( response ) {
               var array = $.map(response, function(value, /* eslint-disable */index/* eslint-enable */) {
                    return [value];
                });

                var newOptions = {
                    "Aucun Cours":"-1",
                };
                newOptions["Toute les Cours"] = "0";

                if (array[0] != "course") {
                    array.forEach(function(element) {
                        newOptions[element["fullname"]] = element["id"];
                      });

                    var last_value = $("#id_course_select").val();
                    var $el = $('#id_course_select');

                    $el.html(' ');
                    $.each(newOptions, function(key, value) {
                        $el.append($("<option></option>")
                        .attr("value", value).text(key));
                    });

                    if (document.getElementById("id_course_select").innerHTML.indexOf('value="' + last_value + '"') > -1) {
                        $("#id_course_select").val(last_value);
                    }
                }
                alert("Vous venez de changer la disponibilité des Metadonnées Partagés");
            },
            error: function( error )
            {
                alert( "Erreur "+error );
            }
        });
    };

        confpage.rec_categ_visibility = function(value, contextlevel) {
        $.ajax({
            url: "categ_visibility.php",
            method: "POST",
            data: { valueinput : value, contextlevel : contextlevel },
            dataType: "json",
            success: function( response ) {
                alert("Vous venez de changer la visibilité de la catégorie selectionnée pour les étudiants à "+ !response);
            },
            error: function( error )
            {
                alert( "Erreur rec_categ_visibility"+error );
            }
        });
    };

    confpage.init = function(contextlevel) {
      $( ".custom-select" ).change(function() {
        if ($(this).attr( "name" ) == "category_select" || $(this).attr( "name" ) == "course_select") {
            confpage.rec_new_categ_filter(this.value,$(this).attr("name"),contextlevel);
        }
      });

      $( "input[name='hiddencateg']").click(function() {
          confpage.rec_categ_visibility($(this).is( ':checked' ), contextlevel);
      });
    };

    /**
    *
    *  Secure Hash Algorithm (SHA1)
    *  http://www.webtoolkit.info/
    *
    **/
    confpage.SHA1 = function(msg) {
            /* eslint-disable */
            function rotate_left(n,s) {
                    var t4 = ( n<<s ) | (n>>>(32-s));
                    return t4;
            }

            function cvt_hex(val) {
                    var str="";
                    var i;
                    var v;

                    for( i=7; i>=0; i-- ) {
                            v = (val>>>(i*4))&0x0f;
                            str += v.toString(16);
                    }
                    return str;
            }


            function Utf8Encode(string) {
                    string = string.replace(/\r\n/g,"\n");
                    var utftext = "";

                    for (var n = 0; n < string.length; n++) {

                            var c = string.charCodeAt(n);

                            if (c < 128) {
                                    utftext += String.fromCharCode(c);
                            }
                            else if((c > 127) && (c < 2048)) {
                                    utftext += String.fromCharCode((c >> 6) | 192);
                                    utftext += String.fromCharCode((c & 63) | 128);
                            }
                            else {
                                    utftext += String.fromCharCode((c >> 12) | 224);
                                    utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                                    utftext += String.fromCharCode((c & 63) | 128);
                            }

                    }

                    return utftext;
            }

            var blockstart;
            var i, j;
            var W = new Array(80);
            var H0 = 0x67452301;
            var H1 = 0xEFCDAB89;
            var H2 = 0x98BADCFE;
            var H3 = 0x10325476;
            var H4 = 0xC3D2E1F0;
            var A, B, C, D, E;
            var temp;

            msg = Utf8Encode(msg);

            var msg_len = msg.length;

            var word_array = new Array();
            for( i=0; i<msg_len-3; i+=4 ) {
                    j = msg.charCodeAt(i)<<24 | msg.charCodeAt(i+1)<<16 |
                    msg.charCodeAt(i+2)<<8 | msg.charCodeAt(i+3);
                    word_array.push( j );
            }

            switch( msg_len % 4 ) {
                    case 0:
                            i = 0x080000000;
                    break;
                    case 1:
                            i = msg.charCodeAt(msg_len-1)<<24 | 0x0800000;
                    break;

                    case 2:
                            i = msg.charCodeAt(msg_len-2)<<24 | msg.charCodeAt(msg_len-1)<<16 | 0x08000;
                    break;

                    case 3:
                            i = msg.charCodeAt(msg_len-3)<<24 | msg.charCodeAt(msg_len-2)<<16 | msg.charCodeAt(msg_len-1)<<8	| 0x80;
                    break;
            }

            word_array.push( i );

            while( (word_array.length % 16) != 14 ) word_array.push( 0 );

            word_array.push( msg_len>>>29 );
            word_array.push( (msg_len<<3)&0x0ffffffff );


            for ( blockstart=0; blockstart<word_array.length; blockstart+=16 ) {
                    for( i=0; i<16; i++ ) W[i] = word_array[blockstart+i];
                    for( i=16; i<=79; i++ ) W[i] = rotate_left(W[i-3] ^ W[i-8] ^ W[i-14] ^ W[i-16], 1);

                    A = H0;
                    B = H1;
                    C = H2;
                    D = H3;
                    E = H4;

                    for( i= 0; i<=19; i++ ) {
                            temp = (rotate_left(A,5) + ((B&C) | (~B&D)) + E + W[i] + 0x5A827999) & 0x0ffffffff;
                            E = D;
                            D = C;
                            C = rotate_left(B,30);
                            B = A;
                            A = temp;
                    }

                    for( i=20; i<=39; i++ ) {
                            temp = (rotate_left(A,5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff;
                            E = D;
                            D = C;
                            C = rotate_left(B,30);
                            B = A;
                            A = temp;
                    }

                    for( i=40; i<=59; i++ ) {
                            temp = (rotate_left(A,5) + ((B&C) | (B&D) | (C&D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff;
                            E = D;
                            D = C;
                            C = rotate_left(B,30);
                            B = A;
                            A = temp;
                    }

                    for( i=60; i<=79; i++ ) {
                            temp = (rotate_left(A,5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff;
                            E = D;
                            D = C;
                            C = rotate_left(B,30);
                            B = A;
                            A = temp;
                    }

                    H0 = (H0 + A) & 0x0ffffffff;
                    H1 = (H1 + B) & 0x0ffffffff;
                    H2 = (H2 + C) & 0x0ffffffff;
                    H3 = (H3 + D) & 0x0ffffffff;
                    H4 = (H4 + E) & 0x0ffffffff;

            }

            var temp = cvt_hex(H0) + cvt_hex(H1) + cvt_hex(H2) + cvt_hex(H3) + cvt_hex(H4);
            return temp.toLowerCase();
            /* eslint-enable */
    };

    window.confpage = confpage;
    return confpage;
 });