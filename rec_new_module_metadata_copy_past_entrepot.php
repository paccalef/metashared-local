<?php

require_once(dirname(__FILE__) . '/../../config.php');
require_once("lib.php");
global $DB, $PAGE;

$data = new stdClass();
foreach ($_POST as $key => $value)
{
    $data->$key = $value;
}

        if ($fields = $DB->get_records('local_metashared_field', array('contextlevel' => 70))) {
            foreach ($fields as $field) {
                // The id received in $data is not the one from course module but from the related module (url, label, etc...) instead.
                // To get the right id and pass it to $formfield so it's saved, a new object is created with the course module id used by the metadata
                $newfield = "\\metasharedfieldtype_{$field->datatype}\\metadata";
                $datachunk = new \stdClass();
                $datachunk->id = $_POST['instance_id'];
                if (isset($data->{'local_metashared_field_' . $field->shortname})) {
                    $datachunk->{'local_metashared_field_' . $field->shortname} = $data->{'local_metashared_field_' . $field->shortname};
                    $formfield = new $newfield($field->id, $datachunk->id);
                    $formfield->edit_save_data($datachunk);
                }
            }
        }
        //Dangerous approch, need to be fixed
               $url_metashared_stuff = $CFG->wwwroot."/mod/".$DB->get_record_sql("SELECT m.name FROM {course_modules} cm INNER JOIN {modules} m ON cm.module = m.id WHERE cm.id=".$_POST['instance_id'], array())->name."/view.php?id=". $_POST['instance_id'];

$shentry = new StdClass;
                   $shentry->title = $data->name;
                   $shentry->type = "";
                   $shentry->mimetype =  "";
                   $shentry->identifier = $data->identifier;
                   $shentry->remoteid = '';
                   $shentry->file = "none";
                   $shentry->url = $url_metashared_stuff;
                   $shentry->lang = $USER->lang; // not more used
                   $shentry->description = "description de ".$data->name;
                   $shentry->keywords = "none";
                   $shentry->timemodified = time();
                   $shentry->provider = 'local';
                   $shentry->isvalid = 1;
                   $shentry->context = 1;
                   $shentry->scoreview = 0;
                   $shentry->scorelike = 0;
                   $shentry->id_instance = $_POST['instance_id'];
               if (empty($DB->get_record('metasharedrc_entry', array("id_instance" => $_POST['instance_id'])))){
                   $DB->insert_record('metasharedrc_entry', $shentry);
               }
redirect($CFG->wwwroot."/course/view.php?id=".$_POST['course_id']);