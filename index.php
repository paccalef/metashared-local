<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package local_metashared
 * @author Mike Churchward <mike.churchward@poetopensource.org>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @copyright 2017, onwards Poet
 */

require('../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->dirroot.'/local/metashared/lib.php');
require_once($CFG->dirroot.'/local/metashared/definelib.php');

$action   = optional_param('action', '', PARAM_ALPHA);
$contextlevel = optional_param('contextlevel', CONTEXT_USER, PARAM_INT);
$redirect = $CFG->wwwroot.'/local/metashared/index.php?contextlevel='.$contextlevel;
//metashared
$strchangessaved    = get_string('changessaved');
$strcancelled       = get_string('cancelled');
$strdefaultcategory = get_string('profiledefaultcategory', 'admin');
$strnofields        = get_string('profilenofieldsdefined', 'admin');
$strcreatefield     = get_string('profilecreatefield', 'admin');

$PAGE->requires->js_call_amd('local_metashared/confpage', 'init', array($contextlevel));

$contextname = local_metashared_get_contextname($contextlevel);

if ($action == $contextname.'data') {
    require_login();
} else {
    admin_externalpage_setup('metasharedcontext_'.$contextname);
}

// Do we have any actions to perform before printing the header.

switch ($action) {
    case 'movecategory':
        $id  = required_param('id', PARAM_INT);
        $dir = required_param('dir', PARAM_ALPHA);

        if (confirm_sesskey()) {
            local_metashared_move_category($id, $dir);
        }
        redirect($redirect);
        break;

    case 'movefield':
        $id  = required_param('id', PARAM_INT);
        $dir = required_param('dir', PARAM_ALPHA);

        if (confirm_sesskey()) {
            local_metashared_move_field($id, $dir);
        }
        redirect($redirect);
        break;

    case 'deletecategory':
        $id      = required_param('id', PARAM_INT);
        if (confirm_sesskey()) {
            local_metashared_delete_category($id);
        }
        redirect($redirect, get_string('deleted'));
        break;

    case 'deletefield':
        $id      = required_param('id', PARAM_INT);
        $confirm = optional_param('confirm', 0, PARAM_BOOL);

        // If no userdata for profile than don't show confirmation.
        $datacount = $DB->count_records('local_metashared', ['fieldid' => $id]);
        if (((data_submitted() && $confirm) || ($datacount === 0)) && confirm_sesskey()) {
            local_metashared_delete_field($id);
            redirect($redirect, get_string('deleted'));
        }

        // Ask for confirmation, as there is user data available for field.
        $fieldname = $DB->get_field('local_metashared_field', 'name', ['id' => $id]);
        $optionsyes = ['id' => $id, 'confirm' => 1, 'action' => 'deletefield', 'sesskey' => sesskey()];
        $strheading = get_string('profiledeletefield', 'admin', format_string($fieldname));
        $PAGE->navbar->add($strheading);
        echo $OUTPUT->header();
        echo $OUTPUT->heading($strheading);
        $formcontinue = new single_button(new moodle_url($redirect, $optionsyes), get_string('yes'), 'post');
        $formcancel = new single_button(new moodle_url($redirect), get_string('no'), 'get');
        echo $OUTPUT->confirm(get_string('profileconfirmfielddeletion', 'admin', $datacount), $formcontinue, $formcancel);
        echo $OUTPUT->footer();
        die;
        break;

    case 'editfield':
        $id       = optional_param('id', 0, PARAM_INT);
        $datatype = optional_param('datatype', '', PARAM_ALPHA);

        local_metashared_edit_field($id, $datatype, $redirect, $contextlevel);
        die;
        break;

    case 'editcategory':
        $id = optional_param('id', 0, PARAM_INT);

        local_metashared_edit_category($id, $redirect, $contextlevel);
        die;
        break;

    default:
        $contextplugins = core_component::get_plugin_list('metasharedcontext');
        if ($action == $contextname.'data') {
            $instanceid = required_param('id', PARAM_INT);
            $contexthandler = \local_metashared\context\context_handler::factory($contextname, $instanceid);

            $instance = $contexthandler->get_instance();
            $layout = $contexthandler->get_layout();
            $context = $contexthandler->get_context();
            $redirect = $contexthandler->get_redirect();
            if (!$contexthandler->require_access()) {
                // Error. No access should be granted.
                die;
            }

            $PAGE->set_url('/local/metashared/index.php',
                ['contextlevel' => $contextlevel, 'id' => $instanceid, 'action' => $action]);
            $PAGE->set_pagelayout($layout);
            $PAGE->set_context($context);

            // Add the metadata to the object.
            local_metashared_load_data($instance, $contextlevel);
            $dataclass = "\\metasharedcontext_{$contextname}\\output\\manage_data";
            $formclass = "\\metasharedcontext_{$contextname}\\output\\manage_data_form";
            $dataoutput = new $dataclass($instance, $contextlevel, $action);
            $dataform = new $formclass(null, $dataoutput);
            $dataoutput->add_form($dataform);

            // Handle form data.
            if ($dataform->is_cancelled()) {
                redirect($redirect);
            } else if (!($data = $dataform->get_data())) {
                $output = $PAGE->get_renderer('metasharedcontext_'.$contextname);
                echo $output->render($dataoutput);
            } else {
                local_metashared_save_data($data, $contextlevel);
                $output = $PAGE->get_renderer('metasharedcontext_'.$contextname);
                $dataoutput->set_saved();
                echo $output->render($dataoutput);
            }
            die;
        } elseif (isset($_GET['working_theme_select'])) {
            $data_conf = new stdClass();
            $data_conf->working_theme_select = intval($_GET['working_theme_select']);
            
            if ($DB->get_records('local_metashared_conf')) {
                $data_conf->id = end($DB->get_records('local_metashared_conf'))->id;
                $DB->update_record('local_metashared_conf', $data_conf);
            } else {
                $DB->insert_record('local_metashared_conf', $data_conf);
            }   
        }
        // Normal form.
        break;
}

// Show all categories.
$categories = $DB->get_records('local_metashared_category', ['contextlevel' => $contextlevel], 'sortorder ASC');

// Check that we have at least one category defined.
if (empty($categories)) {
    $defaultcategory = new stdClass();
    $defaultcategory->contextlevel = $contextlevel;
    $defaultcategory->name = $strdefaultcategory;
    $defaultcategory->sortorder = 1;
    $DB->insert_record('local_metashared_category', $defaultcategory);
    redirect($redirect);
}

$PAGE->set_url($CFG->wwwroot.'/local/metashared/index.php', ['contextlevel' => $contextlevel]);
$output = $PAGE->get_renderer('metasharedcontext_'.$contextname);
// Print the header.
echo $output->header();
echo $output->heading(get_string('metasharedtitle', 'metasharedcontext_'.$contextname));

echo $output->render(new \local_metashared\output\category_table($categories));
echo $output->render(new \local_metashared\output\data_creation($contextlevel));
echo $output->render(new \local_metashared\output\categ_selector($contextlevel));

echo $output->footer();
die;