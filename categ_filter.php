<?php
require('../../config.php');
global $DB;
$current = $DB->get_record('local_metashared_category', ['contextlevel' =>  $_POST['contextlevel']]);

if ($_POST['nameinput'] == "category_select") {
    $sql = "UPDATE {local_metashared_category}
                   SET idcateg=".$_POST['valueinput']." WHERE id=?";
} else {
    $sql = "UPDATE {local_metashared_category}
                   SET idcourse=".$_POST['valueinput']." WHERE id=?";
}



$DB->execute($sql, array($current->id));



if ($_POST['nameinput'] == "category_select") {
    if ($_POST['valueinput'] != 0 ) {
        $param["category"] = $_POST['valueinput']; 
    } else {
        $param = array();
    }
    
    if ($current->idcourse != -1 && $DB->get_record('course',["id" => $current->idcourse]) != false) {
        //If a course is selected but you put "no categ" in select menu
        if ((
                ($_POST['valueinput'] == -1) 
                || 
                (
                        $_POST['valueinput'] != 0
                        &&
                        ($DB->get_record('course',["id" => $current->idcourse])->category 
                        != 
                        $_POST['valueinput'])
                )
            ) 
            &&
            $current->idcourse!=0
           ) {
            $sql = "UPDATE {local_metashared_category}
                       SET idcourse=-1 WHERE id=?";
            $DB->execute($sql, array($current->id));
        }
    }
    $courses = $DB->get_records('course',$param); 
    $myJSON = json_encode($courses);
    
    header("Content-Type: application/json", true);
    echo $myJSON;
} else {
    $answer = array();
    $answer[] = "course";
    header("Content-Type: application/json", true);
    $myJSON = json_encode($answer);
    echo $myJSON;
}