define(['jquery'], function($) {
    var items = [];
    function unset(array, valueOrIndex){
        var output=[];
        for(var i in array) {
            var str_array = array[i].replace(/\n|\r/g, "");

            if (str_array!=valueOrIndex) {
                output.push(str_array);
            }
        }
        return output;
    }

    function add_value_to_hidden_input(name) {
        var returned_values = "";

                items[name].forEach(function(elem) {
                    if (returned_values != '') {
                        returned_values = returned_values + "\n" +elem;
                    } else {
                        returned_values = returned_values + elem;
                    }
                });

                $('input[name='+name+']').attr('value', returned_values);
    }
    return {
        init: function() {
           $('body').on('DOMNodeInserted', '.selected_m_item', function () {
                $('.selected_m_item').click(function(){
                   items[$(this).attr("field")] = unset(items[$(this).attr("field")],$(this).attr("id"));
                   $(this).remove();
                   add_value_to_hidden_input($(this).attr("field"));
            });
                        });
          $('.clone-input').change(function(){
                var select_value = String($(this).val());

                if (items[this.name.substr(0,this.id.length-9)] !== undefined
                   /*items[this.name.substr(0,this.id.length-9)].length != 0*/) {
                    var allready = false;

                    items[this.name.substr(0,this.id.length-9)].forEach(function(element) {
                       if (select_value == element) {
                           allready = true;
                       }
                    });
                    if (allready === false && select_value!="") {
                        $('#selected-item-'+this.name.substr(0,this.id.length-9)).append(
                                "<span field='"+this.name.substr(0,this.id.length-9)+
                                "' id='"+select_value+
                                "' class='selected_m_item tag tag-info mb-3 mr-1' style='cursor: pointer;'>"+
                                "<span aria-hidden='true'>× </span>"
                                +select_value+"</span>");
                        items[this.name.substr(0,this.id.length-9)].push(select_value);
                    }
                } else {
                        $('#selected-item-'+this.name.substr(0,this.id.length-9)).html(
                                "<span field='"+this.name.substr(0,this.id.length-9)+
                                "' id='"+select_value+
                                "' class='selected_m_item tag tag-info mb-3 mr-1' style='cursor: pointer;'>"+
                                "<span aria-hidden='true'>×</span>"
                                +select_value+"</span>");
                        items[this.name.substr(0,this.id.length-9)]=[];
                        items[this.name.substr(0,this.id.length-9)].push(select_value);
                }

                var name = this.name.substr(0,this.id.length-9);

                /*--add the categ value--*/
                var allready_categ = false;
                var selector = this.name.substr(0,this.id.length-9);

                items[this.name.substr(0,this.id.length-9)].forEach(function(element) {
                    if ($("#"+selector).val() == element) {
                       allready_categ = true;
                    }
                });
                if (!allready_categ) {
                    $('#selected-item-'+selector).append(
                                "<span field='"+selector+
                                "' id='"+$("#"+selector).val()+
                                "' class='selected_m_item tag tag-info mb-3 mr-1' style='cursor: pointer;'>"+
                                "<span aria-hidden='true'>×</span>"
                                +$("#"+selector).val()+"</span>");
                    items[selector].push($("#"+selector).val());
                }

                add_value_to_hidden_input(name);
            });

            $(".categ-selector").change(function() {
                 $('.'+this.id).css("display", "none");
                 $('.'+this.value.replace(/\s/g, '')).css("display", "block");
                // $('.'+this.value.replace(/\s/g, '')+' :selected').removeAttr("selected");
              //  $('input[name='+this.id+']').attr('value', $('.'+this.value.replace(/\s/g, '')+" option:first").val());
                 //$('.Categ1 :selected').val();.removeAttr("selected");
            });

            $( ".selected_m_item" ).click(function() {
                items[$(this).attr("field")] = unset(items[$(this).attr("field")],$(this).attr("id"));
                $(this).remove();
                add_value_to_hidden_input($(this).attr("field"));
          });
        },set_data: function(name, data) {
            items[name]=[];
            data.forEach(function(element) {
                items[name].push(element);
            });
        }
    };
});