<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_metashared\output;
use moodleform;

defined('MOODLE_INTERNAL') || die();

/**
 * Class field_form
 *
 * @copyright  2007 onwards Shane Elliot {@link http://pukunui.com}
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class categ_perimeter_form extends moodleform {
    public function definition () {
        global $DB;
        $mform = $this->_form;
        
         $course_categories = $DB->get_records('course_categories'); 
         $tcc= array();
         $tcc[-1] = "Aucune Catégorie";
         $tcc[0] = "Toute les catégories";
         foreach ($course_categories as $cc) {
             $tcc[$cc->id] = /*array("id" => $cc->id, "name" => */$cc->name/*)*/;
         }
         $current = $DB->get_record('local_metashared_category', ['contextlevel' =>  $_GET['contextlevel']]);
         
         $mform->addElement('html', '<table><tr><td>');
         $select = $mform->addElement('select', 'category_select', "", $tcc);
         if ($current->idcateg == NULL) {
             $select->setSelected(0);
         } else {
             $select->setSelected($current->idcateg);
         }
         $mform->addElement('html', '</td><td>');
         $hiddencateg = $mform->addElement('advcheckbox', 'hiddencateg');
         
         if ($current->hiddencateg == 1) {
            $hiddencateg->setChecked(true);
         } else {
             $hiddencateg->setChecked(false);
         }
         
         $mform->addElement('html', '</td><td><span id="categ_perimeter_hidden_categ_title" style="bottom: 7px;position: relative;left: -28px;">Caché</span>');
        
       
         $param =array();
         
         if ($current->idcateg != 0 && $current->idcateg != -1) {
             $param["category"] = $current->idcateg;
         }
         
         $courses = $DB->get_records('course',$param); 
         $tcs = array();
         $tcs[-1] = "Aucun Cours";
         $tcs[0] = "Tout les cours";
         
         if ($current->idcateg != -1) {
            foreach ($courses as $cs) {
                $tcs[$cs->id] = /*array("id" => $cs->id, "name" => */$cs->fullname/*)*/;
            }
         }
         $mform->addElement('html', '</td><td>');
         $select = $mform->addElement('select', 'course_select', "", $tcs);
         if ($current->idcourse == NULL) {
             $select->setSelected(0);
         } else {
             $select->setSelected($current->idcourse);
         }
         $mform->addElement('html', '</td></tr></table>');

    }
}
